local function print(s)
    if type(s) ~= "string" then
        s = tostring(s)
    end
    DEFAULT_CHAT_FRAME:AddMessage(s)
end
local DemoWindow
local function UIInit()
    DemoWindow = EFrame.Window()
    DemoWindow.visible = false
    DemoWindow.title = "EmeraldFramework Demo"
    DemoWindow.centralItem = EFrame.ColumnLayout(DemoWindow)
    DemoWindow.centralItem.spacing = 2
    
    local label = EFrame.Label(DemoWindow.centralItem)
    label.text = "Random Label"
    -- Buttons
    local ButtonsRow = EFrame.RowLayout(DemoWindow.centralItem)
    ButtonsRow.spacing = 2
    local button = EFrame.Button(ButtonsRow)
    button.text = "Button"
    button.clicked:connect(print)
    local toggable = EFrame.Button(ButtonsRow)
    toggable.checkable = true
    toggable.text = EFrame.bind("(self.containsPress or self.checked) and 'Pushed' or 'Released'")
    -- CheckButton
    local checkbutton = EFrame.CheckButton(DemoWindow.centralItem)
    checkbutton.text = "Show radio buttons"
    -- RadioButtons
    local RadioRow = EFrame.RowLayout(DemoWindow.centralItem)
    RadioRow.spacing = 2
    RadioRow.visible = checkbutton._checked
    local ex = EFrame.ExclusiveGroup(RadioRow)
    local radio1 = EFrame.RadioButton(RadioRow)
    radio1.exclusiveGroup = ex
    radio1.checked = true
    radio1.text = "Option1"
    local radio2 = EFrame.RadioButton(RadioRow)
    radio2.exclusiveGroup = ex
    radio2.text = "Option2"
    ex.onCurrentChanged = function(_, current) print(current.text) end
    
    DemoWindow.onVisibleChanged = function(self, v) if not v then DemoWindow:deleteLater() DemoWindow = nil end end
end

local function chatcmd()
    if not DemoWindow then
        UIInit()
    end
    DemoWindow.visible = not DemoWindow.visible
end

SLASH_EFRAMEDEMO1 = "/efd"
SlashCmdList["EFRAMEDEMO"] = chatcmd
